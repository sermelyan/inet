#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h> 

   
int main(int argc, char const *argv[]) 
{ 
    struct sockaddr_in address; 
    int sock = 0; 
    struct sockaddr_in serv_addr;

    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) { 
        printf("\n Socket creation error \n"); 
        return -1; 
    } 
   
    memset(&serv_addr, '0', sizeof(serv_addr)); 
   
    serv_addr.sin_family = AF_INET; 
    serv_addr.sin_port = htons(atoi(argv[1])); 

    if(inet_pton(AF_INET, argv[2], &serv_addr.sin_addr)<=0) { 
        printf("\nInvalid address/ Address not supported \n"); 
        return -1; 
    } 
   
    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) { 
        printf("\nConnection Failed \n"); 
        return -1; 
    }

    char buffer[1024];

    while(scanf("%s", buffer) != EOF) {
        if(send(sock , buffer , strlen(buffer) , 0 )) {
            printf("message sent\n");
        }
    }

    return 0; 
} 